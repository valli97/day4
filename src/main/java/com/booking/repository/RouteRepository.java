package com.booking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.booking.model.Route;

@Repository
public interface RouteRepository extends JpaRepository<Route, Integer> {

}


