package com.booking.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import com.booking.dto.RouteDto;
import com.booking.exceptions.AdminNotFoundException;
import com.booking.model.Route;
import com.booking.model.User;
import com.booking.repository.RouteRepository;
import com.booking.repository.UserRepository;

@Service
public class RouteService {
	@Autowired
	private RouteRepository routeRepository;
	@Autowired
	private UserRepository userRepository;
	Route route = new Route();

	// list
	/*
	 * public Route getAllRoutes() throws AdminNotFoundException { //
	 * BeanUtils.copyProperties(routeDto,route); User user =
	 * userRepository.findByEmailId(emailId); Route route = null; if
	 * (user.getProfile().equals("admin")) { List<Route> route1=
	 * routeRepository.findAll(); }else { throw new
	 * AdminNotFoundException("Authorised To Admin Only"); } return route; }
	 */

	public List<Route> getAllRoutes() throws AdminNotFoundException {
		// BeanUtils.copyProperties(routeDto,route);
		// User user = userRepository.findByEmailId(emailId);

		User user = new User();

		if (user.getProfile().equals("admin")) {
			List<Route> route1 = routeRepository.findAll();
		} else {
			throw new AdminNotFoundException("Authorised To Admin Only");
		}
		return null;

	}

	// create
	public Route saveRoute(RouteDto routeDto, String emailId) throws AdminNotFoundException {
		BeanUtils.copyProperties(routeDto, route);
		User user = userRepository.findByEmailId(emailId);
		Route route = null;
		if (user.getProfile().equals("admin")) {
			route = routeRepository.save(route);
		} else {
			throw new AdminNotFoundException("Authorised To Admin Only");
		}
		return route;
	}

	// byid
	public Route getRouteById(RouteDto routeDto, String emailId, int routeId) throws AdminNotFoundException {
		BeanUtils.copyProperties(routeDto, route);
		User user = userRepository.findByEmailId(emailId);
		Route route = null;
		if (user.getProfile().equals("admin")) {

			route = routeRepository.getOne(routeId);
		} else {
			throw new AdminNotFoundException("Authorised To Admin Only");
		}
		return route;
	}

	// update
	public Route updateRoute(RouteDto routeDto, String emailId) throws AdminNotFoundException {
		BeanUtils.copyProperties(routeDto, route);
		User user = userRepository.findByEmailId(emailId);
		Route route = null;
		if (user.getProfile().equals("admin")) {
			route = routeRepository.save(route);
		} else {
			throw new AdminNotFoundException("Authorised To Admin Only");
		}
		return route;
	}

	// delete
	public Route deleteRouteById(String emailId, int routeId) throws AdminNotFoundException {
		// BeanUtils.copyProperties(routeDto, route);
		User user = userRepository.findByEmailId(emailId);
		Route route = null;
		if (user.getProfile().equals("admin")) {
			routeRepository.deleteById(routeId);
		} else {
			throw new AdminNotFoundException("Authorised To Admin Only");
		}
		return route;
	}

	/*
	 * public Route deleteRouteById(RouteDto routeDto, String emailId,int routeId)
	 * throws AdminNotFoundException { BeanUtils.copyProperties(routeDto, route);
	 * User user = userRepository.findByEmailId(emailId); Route route = null; if
	 * (user.getProfile().equals("admin")) { route =
	 * routeRepository.deleteById(routeId); } else { throw new
	 * AdminNotFoundException("Authorised To Admin Only"); } return route; }
	 */
}
