package com.booking.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.booking.dto.BusDeatilsDto;
import com.booking.exceptions.NoDataFoundException;
import com.booking.model.BusDetails;
import com.booking.repository.BusDetailsRepository;

@Service
public class BusDetailsService {

	@Autowired
	BusDetailsRepository busDetailsRepository;

	BusDetails busDetails = new BusDetails();

	public BusDetails searchDetails(BusDeatilsDto busDetailsDto) {
		BeanUtils.copyProperties(busDetailsDto, busDetails);
		List<BusDetails> bus = busDetailsRepository.findBySourceAndDestinationAndDay(busDetailsDto.getSource(),
				busDetailsDto.getDestination(), busDetailsDto.getDay());
		if (bus==null) {
			throw new NoDataFoundException("Enter valid BUS DETAILS");
		} else {
			return busDetails;
		}

	}

	public BusDetails saveBusDetails(BusDeatilsDto busDetailsDto) {
		BeanUtils.copyProperties(busDetailsDto, busDetails);
		if (busDetails.getAvailableTickets() > 40) {
			throw new NoDataFoundException("Enter valid BUS DETAILS");
		} else {
			return busDetailsRepository.save(busDetails);

		}

	}

}
