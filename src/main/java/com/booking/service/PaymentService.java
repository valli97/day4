package com.booking.service;

import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.booking.model.BusDetails;
import com.booking.model.Payment;
import com.booking.model.TicketBooking;
import com.booking.model.Transaction;
import com.booking.model.User;
import com.booking.repository.BusDetailsRepository;
import com.booking.repository.PaymentRepository;
import com.booking.repository.TicketBookingRepository;
import com.booking.repository.UserRepository;
import com.booking.dto.TransferDto;

@Service
public class PaymentService {
	
	@Autowired 
	private PaymentRepository   paymentRepository;
	
	@Autowired
	private UserRepository    userRepository;
	
	@Autowired
	private BusDetailsRepository busDetailsRepository;
	
	@Autowired
	private TicketBookingRepository ticketBookingRepository;
	
	TicketBooking ticketBooking =new TicketBooking();
	
	@Autowired
	RestTemplate restTemplate;
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	public ResponseEntity<TicketBooking> paymentForTicketBooking(String username, int busId,int ticketId,TransferDto transferDto) {
		BusDetails busDetails=busDetailsRepository.getOne(busId);
		User user=userRepository.findByEmailId(username);
		Optional<TicketBooking> optionalTicket=ticketBookingRepository.findById(ticketBooking.getTicketId());
		TicketBooking ticketBooking=new TicketBooking();
		if(optionalTicket.isPresent()) {
			ticketBooking=optionalTicket.get();
			
		}
		
		if((ticketBooking!=null)&&(user!=null) && (busDetails!=null)) {
			String uri = "http://localhost:9016/transfer";

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			JSONObject request = new JSONObject();
			request.put("savingsAccountNumber",transferDto.getUserAccountNumber());
			request.put("beneficiaryAccountNumber", transferDto.getRedbusAccountNumber());
			request.put("transferAmmount", ticketBooking.getTicketCharges());

			HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

			ResponseEntity<Transaction> response = restTemplate.postForEntity(uri, entity, Transaction.class);

			System.out.println(response);
			return new ResponseEntity<TicketBooking>(ticketBooking, HttpStatus.OK);
		}
		
		return null;
	}
	


}
	


