package com.booking.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.booking.dto.TicketBookingDto;
import com.booking.dto.TicketDto;
import com.booking.model.BusDetails;
import com.booking.model.TicketBooking;
import com.booking.model.User;
import com.booking.repository.BusDetailsRepository;
import com.booking.repository.TicketBookingRepository;
import com.booking.repository.UserRepository;

@Service
public class TicketBookingService {

	@Autowired
	private TicketBookingRepository ticketBookingRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	BusDetailsRepository busDetailsRepository;

	TicketBookingDto ticket = new TicketBookingDto();

	public TicketBookingDto bookTheSeat(TicketDto ticketDto) {

		TicketBooking ticketBooking = new TicketBooking();
		BeanUtils.copyProperties(ticketDto, ticketBooking);

		Optional<User> optional = userRepository.findById(ticketDto.getUserId());
		User user = null;
		if (optional.isPresent()) {
			user = optional.get();
		}
		if (user != null) {

			Optional<BusDetails> option = busDetailsRepository.findById(ticketDto.getBusId());
			BusDetails bus = null;
			if (option.isPresent()) {
				bus = option.get();
			}
			bus.setAvailableTickets(bus.getAvailableTickets() - ticketDto.getNumberofSeat());
			busDetailsRepository.save(bus);
			ticketBooking.setUser(user);
			ticketBooking.setBusDetails(bus);
			ticketBooking.setNumberofSeat(ticketDto.getNumberofSeat());

			TicketBooking booking1 = ticketBookingRepository.save(ticketBooking);
			ticket.setNumberofSeat(booking1.getNumberofSeat());
			ticket.setReservationDate(booking1.getReservationDate());
			ticket.setTicketNumber(booking1.getTicketNumber());
			ticket.setTicketId(booking1.getTicketId());

		}
		return ticket;

	}

	public TicketBooking passengerDetails(Integer ticketId) {
		return ticketBookingRepository.getOne(ticketId);

	}
}
