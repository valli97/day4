package com.booking.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.booking.dto.UserLoginDto;
import com.booking.exceptions.InvalidCredentialException;
import com.booking.exceptions.UserNotFoundException;
import com.booking.model.User;
import com.booking.repository.UserRepository;

@Service
public class UserLoginService {
	@Autowired
	UserRepository userRepository;

	private User userRegistration = new User();

	public Optional<User> checkLoginByUserId(UserLoginDto userlogindto) {
		BeanUtils.copyProperties(userlogindto, userRegistration);
		Optional<User> login = userRepository.findByEmailIdAndPassword(userlogindto.getEmailId(),
				userlogindto.getPassword());
		if (!login.isPresent()) {
			throw new InvalidCredentialException(" check login details ");

		} else {
			return login;

		}
	}

	public ResponseEntity<User> userLogin1(UserLoginDto userlogindto1) {
		BeanUtils.copyProperties(userlogindto1, userRegistration);
		User login1 = userRepository.findByEmailId(userlogindto1.getEmailId());
		if (login1 == null) {
			throw new InvalidCredentialException(" check login details ");
		}
		User loginSuccessful = (User) login1;
		loginSuccessful.setIsLogin(true);
		userRepository.save(loginSuccessful);
		return new ResponseEntity<User>(loginSuccessful, HttpStatus.OK);
	}

	public boolean checkLoginValidation(String emailId) {
		// BeanUtils.copyProperties(userlogindto2, userRegistration);
		User login2 = userRepository.findByEmailId(emailId);
		if (login2 == null) {
			throw new InvalidCredentialException(" check login details ");
		}
		User loginSuccessful = (User) login2;
		if (loginSuccessful.getIsLogin() == false) {
			throw new InvalidCredentialException(" Not Logged ");
		}
		if (loginSuccessful.getIsLogin() == true) {
			return true;
		}
		return false;
	}
}
