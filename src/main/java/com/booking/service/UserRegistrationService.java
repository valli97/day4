package com.booking.service;

import java.util.Optional;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.booking.dto.UserRegistrationDto;
import com.booking.exceptions.ResourceNotFoundException;
import com.booking.model.User;
import com.booking.repository.UserRepository;

@Service
public class UserRegistrationService {

	@Autowired
	UserRepository userRepository;

	User user = new User();

	public User saveUserRegistration(UserRegistrationDto userRegistrationDto) {
		BeanUtils.copyProperties(userRegistrationDto, user);
		return userRepository.save(user);

	}

	public User passengerDetails(Integer userId) {
		Optional<User> user1 = userRepository.findById(userId);
		if (user1.isPresent()) {
			return user1.get();
		} else {
			throw new ResourceNotFoundException("Invalid User Id ::" + userId);
		}
	}

}
