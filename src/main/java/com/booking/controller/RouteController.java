package com.booking.controller;

import java.util.List;

import javax.net.ssl.SSLEngineResult.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.booking.dto.ResponseDto;
import com.booking.dto.RouteDto;
import com.booking.dto.UserLoginDto;
import com.booking.exceptions.AdminNotFoundException;
import com.booking.model.Route;
import com.booking.service.RouteService;
import com.booking.service.UserLoginService;

@RestController
public class RouteController {
	@Autowired
	RouteService routeService;
	ResponseDto responseDto = new ResponseDto();
	@Autowired
	UserLoginService userLoginService;

	@PostMapping("/route1")

	public ResponseEntity<ResponseDto> route(@RequestBody RouteDto routeDto, @RequestParam String emailId)
			throws AdminNotFoundException {
		Route route = routeService.saveRoute(routeDto, emailId);
		responseDto.setMessage("Route Created Successfully");
		return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
	}

	/*
	 * @GetMapping("/route2") public List<Route> getAll(@RequestBody RouteDto
	 * routeDto,@RequestParam String emailId) throws AdminNotFoundException {
	 * List<Route> route=routeService.getAllRoutes(routeDto,emailId);
	 * responseDto.setMessage("List Of Routes"); return new
	 * ResponseEntity<>(responseDto, HttpStatus.OK); }
	 */

	@DeleteMapping(value = "/route3/{id}")
	public ResponseEntity<ResponseDto> deleteRoute(@RequestParam String emailId, @PathVariable int routeId)
			throws AdminNotFoundException {
		responseDto.setMessage("Route deleted Successfully");
		Route route = routeService.deleteRouteById(emailId, routeId);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@PutMapping(value = "/route4")
	public ResponseEntity<ResponseDto> updateRoute(@RequestBody RouteDto routeDto, @RequestParam String emailId)
			throws AdminNotFoundException {
		Route route = routeService.updateRoute(routeDto, emailId);
		responseDto.setMessage("Route Updated Successfully");
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

	@GetMapping("/routeList/{emailId}")
	public ResponseEntity<List<Route>> RouteDetailLists(@RequestParam("emailId") String emailId)
			throws AdminNotFoundException {

		responseDto.setMessage("View The Route Details ");
		List<Route> routeList =null;
		if (userLoginService.checkLoginValidation(emailId)) {
			 routeList = (List<Route>) routeService.getAllRoutes();
			return new ResponseEntity<List<Route>>(routeList, HttpStatus.OK);
		} else {
			throw new AdminNotFoundException("not logged");
		}

	}

}
