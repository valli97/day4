package com.booking.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.booking.dto.ResponseDto;
import com.booking.dto.UserLoginDto;
import com.booking.model.User;
import com.booking.service.UserLoginService;

@RestController
public class UserLoginController {
	@Autowired
	private UserLoginService userloginService;
	ResponseDto responseDto = new ResponseDto();

	@PostMapping("/checklogin")
	public ResponseEntity<ResponseDto> checkLoginByUserId(@RequestBody UserLoginDto userloginDto) {

		Optional<User> userDto = userloginService.checkLoginByUserId(userloginDto);

		responseDto.setMessage("User Login Successful");

		return new ResponseEntity<>(responseDto, HttpStatus.ACCEPTED);
	}

	@PostMapping("/login1")
	public ResponseEntity<ResponseDto> productLogin(@RequestBody UserLoginDto user) {
		userloginService.userLogin1(user);
		responseDto.setMessage("Admin  Login Successful");
		return new ResponseEntity<>(responseDto, HttpStatus.ACCEPTED);
	}

}
