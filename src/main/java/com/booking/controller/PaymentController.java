package com.booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.booking.dto.TransferDto;
import com.booking.model.TicketBooking;
import com.booking.service.PaymentService;


@RestController
public class PaymentController {

	@Autowired
	PaymentService paymentService;

	@PostMapping("/payment")
	public ResponseEntity<TicketBooking> BusPayment(@RequestParam String email, @RequestParam int busId,
			@RequestParam int ticketId, @RequestBody TransferDto transferDto) {
		return paymentService.paymentForTicketBooking(email, busId, ticketId, transferDto);

	}

}
