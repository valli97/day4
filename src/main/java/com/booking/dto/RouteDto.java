package com.booking.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RouteDto {
	@JsonProperty
	private Integer routeId;
	@JsonProperty
	private String source;
	@JsonProperty
	private String destination;
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Integer getRouteId() {
		return routeId;
	}
	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}
	
	
	
	
	
}
