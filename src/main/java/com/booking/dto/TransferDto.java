package com.booking.dto;

public class TransferDto {
	
	private long userAccountNumber;
	private long redbusAccountNumber;
	private double price;
	public long getUserAccountNumber() {
		return userAccountNumber;
	}
	public void setUserAccountNumber(long userAccountNumber) {
		this.userAccountNumber = userAccountNumber;
	}
	
	public long getRedbusAccountNumber() {
		return redbusAccountNumber;
	}
	public void setRedbusAccountNumber(long redbusAccountNumber) {
		this.redbusAccountNumber = redbusAccountNumber;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public TransferDto(long userAccountNumber, long redbusAccountNumber, double price) {
		super();
		this.userAccountNumber = userAccountNumber;
		this.redbusAccountNumber = redbusAccountNumber;
		this.price = price;
	}
	public TransferDto() {
		super();
	}
	@Override
	public String toString() {
		return "TransferDto [userAccountNumber=" + userAccountNumber + ", redbusAccountNumber=" + redbusAccountNumber
				+ ", price=" + price + "]";
	}
	
}
