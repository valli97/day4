package com.booking.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserLoginDto {
	@JsonProperty
	private String emailId;
	@JsonProperty
	private String password;

	@JsonProperty
	private Boolean isLogin;
	@JsonProperty
	private String profile;

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	

}
