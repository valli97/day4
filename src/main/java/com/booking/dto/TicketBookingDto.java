package com.booking.dto;

import javax.persistence.Column;

public class TicketBookingDto {

	private Integer ticketId;

	private String reservationDate;

	private Integer ticketNumber;
	
	private Integer numberofSeat;

	public Integer getNumberofSeat() {
		return numberofSeat;
	}

	public void setNumberofSeat(Integer numberofSeat) {
		this.numberofSeat = numberofSeat;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public String getReservationDate() {
		return reservationDate;
	}

	public void setReservationDate(String reservationDate) {
		this.reservationDate = reservationDate;
	}

	public Integer getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(Integer ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	
	

}
