package com.booking.dto;



import com.fasterxml.jackson.annotation.JsonProperty;

public class UserRegistrationDto {
	@JsonProperty
	private long userId;
	@JsonProperty
	private String userName;

	@JsonProperty
	private String emailId;
	@JsonProperty
	private String password;
	@JsonProperty
	private String mobileNumber;
	@JsonProperty
	private Boolean isLogin;
	@JsonProperty
	private String profile;

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public Boolean getIsLogin() {
		return isLogin;
	}
	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}
	/*
	 * @JsonProperty private String message;
	 */
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	}
	

