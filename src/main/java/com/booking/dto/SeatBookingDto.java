package com.booking.dto;



import com.fasterxml.jackson.annotation.JsonProperty;

public class SeatBookingDto {
@JsonProperty
private Long seatid;
@JsonProperty
private String type;
public Long getSeatid() {
	return seatid;
}
public void setSeatid(Long seatid) {
	this.seatid = seatid;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}


}
