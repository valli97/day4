package com.booking.serviceTest;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.booking.dto.RouteDto;
import com.booking.model.Route;
import com.booking.repository.RouteRepository;
import com.booking.service.RouteService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RouteServiceTest {

	@InjectMocks
	RouteService routeService;

	@Mock
	RouteRepository routeRepository;

	@Test
	public void TestGetAllRoutesForPositive() {

		List<Route> route1 = new ArrayList<>();
		Route route = new Route();
		route.setDestination("chennai");
		route.setRouteId(1);
		route.setSource("bangalore");
		route1.add(route);

		Mockito.when(routeRepository.findAll()).thenReturn(route1);

		List<Route> result = routeService.getAllRoutes();
		assertNotNull(result);

	}

	@Test
	public void TestSaveRouteForPositive() {

		Route route = new Route();
		route.setDestination("chennai");
		route.setRouteId(1);
		route.setSource("bangalore");

		RouteDto routedto = new RouteDto();
		routedto.setDestination("chennai");
		routedto.setSource("bangalore");

		Mockito.when(routeRepository.save(Mockito.any())).thenReturn(route);

		Route result = routeService.saveRoute(routeDto, emailId);

		assertNotNull(result);
	}

	@Test
	public void TestUpdateRouteForPositive() {

		Route route = new Route();
		route.setDestination("chennai");
		route.setRouteId(1);
		route.setSource("bangalore");

		RouteDto routedto = new RouteDto();
		routedto.setDestination("chennai");
		routedto.setSource("bangalore");

		Mockito.when(routeRepository.save(Mockito.any())).thenReturn(route);

		Route result = routeService.updateRoute(routedto);

		assertNotNull(result);
	}

	@Test
	public void TestGetRouteByIdForPositive() {

		Route route = new Route();
		route.setDestination("chennai");
		route.setRouteId(1);
		route.setSource("bangalore");

		Mockito.when(routeRepository.getOne(Mockito.anyInt())).thenReturn(route);

		Route result1 = routeService.getRouteById(Mockito.anyInt());

		assertEquals(1, result1.getRouteId());

	}

}
