package com.booking.serviceTest;

 

import static org.junit.Assert.assertNotNull;

 

import java.util.ArrayList;
import java.util.Optional;

 

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

 

import com.booking.dto.TicketBookingDto;
import com.booking.dto.TicketDto;
import com.booking.model.BusDetails;
import com.booking.model.TicketBooking;
import com.booking.model.User;
import com.booking.repository.BusDetailsRepository;
import com.booking.repository.TicketBookingRepository;
import com.booking.repository.UserRepository;
import com.booking.service.TicketBookingService;

 

@RunWith(MockitoJUnitRunner.Silent.class)
public class TicketBookingServiceTest {

 

    @InjectMocks
    TicketBookingService ticketBookingService;

 

    @Mock
    TicketBookingRepository ticketBookingRepository;
    
    @Mock
    UserRepository userRepository;

 

    @Mock
    BusDetailsRepository busDetailsRepository;

 

    @Test
    public void TestBookTheSeatForPositive() {

 

        TicketBookingDto ticket = new TicketBookingDto();
        ticket.setNumberofSeat(25);
        ticket.setReservationDate("2020-12-03");
        ticket.setTicketId(1);
        ticket.setTicketNumber(5);

 

        TicketDto ticket1 = new TicketDto();
        ticket1.setBusId(1);
        ticket1.setNumberofSeat(40);
        ticket1.setReservationDate("2020-12-03");
        ticket1.setTicketId(1);
        ticket1.setTicketNumber(5);
        ticket1.setUserId(1);

 

        TicketBooking ticketbooking = new TicketBooking();
        ticketbooking.setNumberofSeat(5);
        ticketbooking.setReservationDate("2020-12-03");
        ticketbooking.setTicketId(1);
        ticketbooking.setTicketNumber(5);
    
        User user = new User();
        user.setEmailId("dhaya@gmail.com");
        user.setMobileNumber("86526363723");
        user.setPassword("dhaya789");
        user.setUserId(1);
        user.setUserName("dhayananthan");
    
        BusDetails details1=new BusDetails();
        details1.setArrivalDate("2020-12-06");
        details1.setAvailableTickets(10);
        details1.setBusId(1);
        details1.setDepartureDate("2020-12-03");
        details1.setDestination("chennai");
        details1.setSource("bangalore");
        details1.setTotalSeat(45);

 

        Mockito.when(busDetailsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(details1));
        Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(user));
        Mockito.when(busDetailsRepository.save(Mockito.any())).thenReturn(details1);
        Mockito.when(ticketBookingRepository.save(Mockito.any())).thenReturn(ticketbooking);
        
        TicketBookingDto result=ticketBookingService.bookTheSeat(ticket1);
        
        assertNotNull(result);
    }
    
    @Test
    public void TestBookTheSeatForNegative() {

 

        TicketBookingDto ticket = new TicketBookingDto();
        ticket.setNumberofSeat(25);
        ticket.setReservationDate("2020-12-03");
        ticket.setTicketId(1);
        ticket.setTicketNumber(5);

 

        TicketDto ticket1 = new TicketDto();
        ticket1.setBusId(1);
        ticket1.setNumberofSeat(40);
        ticket1.setReservationDate("2020-12-03");
        ticket1.setTicketId(1);
        ticket1.setTicketNumber(5);
        ticket1.setUserId(1);

 

        TicketBooking ticketbooking = new TicketBooking();
        ticketbooking.setNumberofSeat(5);
        ticketbooking.setReservationDate("2020-12-03");
        ticketbooking.setTicketId(1);
        ticketbooking.setTicketNumber(5);
    
        User user = new User();
        user.setEmailId("dhaya@gmail.com");
        user.setMobileNumber("86526363723");
        user.setPassword("dhaya789");
        user.setUserId(2);
        user.setUserName("dhayananthan");
    
        BusDetails details1=new BusDetails();
        details1.setArrivalDate("2020-12-06");
        details1.setAvailableTickets(10);
        details1.setBusId(1);
        details1.setDepartureDate("2020-12-03");
        details1.setDestination("chennai");
        details1.setSource("bangalore");
        details1.setTotalSeat(45);

 

        Mockito.when(busDetailsRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(details1));
        Mockito.when(userRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(user));
        Mockito.when(busDetailsRepository.save(Mockito.any())).thenReturn(details1);
        Mockito.when(ticketBookingRepository.save(Mockito.any())).thenReturn(ticketbooking);
        
        TicketBookingDto result=ticketBookingService.bookTheSeat(ticket1);
        
        assertNotNull(result);
    }

}
