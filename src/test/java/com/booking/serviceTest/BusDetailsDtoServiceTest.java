package com.booking.serviceTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.booking.dto.BusDeatilsDto;
import com.booking.model.BusDetails;
import com.booking.repository.BusDetailsRepository;
import com.booking.service.BusDetailsService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BusDetailsDtoServiceTest {
	@InjectMocks
	BusDetailsService busdetailsservice;

	@Mock
	BusDetailsRepository busDetailsRepository;

	@Test
	public void searchDetailsTest() {

		BusDeatilsDto bus = new BusDeatilsDto();
		bus.setDay("friday");
		bus.setDestination("pune");
		bus.setSource("rajahmundry");
		Mockito.when(busDetailsRepository.findBySourceAndDestinationAndDay(bus.getSource(), bus.getDestination(),
				bus.getDay()));
		Assert.assertEquals("rajahmundry", bus.getSource());

	}

	@Test
	public void TestSaveBusDetailsForPositive() {

		BusDetails busdetails2 = new BusDetails();
		busdetails2.setBookingId(2);
		busdetails2.setBusName("aiyaappa");
		busdetails2.setBusNo("7824");
		busdetails2.setDay("wednesday");
		busdetails2.setDestination("erode");
		busdetails2.setSeatsAvailable(5);
		busdetails2.setSource("trip");

		BusDeatilsDto busdto = new BusDeatilsDto();
		busdto.setDay("wednesday");
		busdto.setDestination("erode");
		busdto.setSource("trip");

		Mockito.when(busDetailsRepository.save(busdetails2)).thenReturn(busdetails2);

	}

	@Test
	public void TestSaveBusDetailsForNegative() {

		BusDetails busdetails2 = new BusDetails();
		busdetails2.setBookingId(2);
		busdetails2.setBusName("aiyaappa");
		busdetails2.setBusNo("7824");
		busdetails2.setDay("wednesday");
		busdetails2.setDestination("erode");
		busdetails2.setSeatsAvailable(45);
		busdetails2.setSource("trip");

		BusDeatilsDto busdto = new BusDeatilsDto();
		busdto.setDay("wednesday");
		busdto.setDestination("erode");
		busdto.setSource("trip");

		Mockito.when(busDetailsRepository.save(busdetails2)).thenReturn(busdetails2);

	}
}
