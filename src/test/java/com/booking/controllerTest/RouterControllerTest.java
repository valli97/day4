package com.booking.controllerTest;

 

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

 

import java.util.ArrayList;
import java.util.List;

 

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

 

import com.booking.controller.RouteController;
import com.booking.dto.ResponseDto;
import com.booking.dto.RouteDto;
import com.booking.exceptions.AdminNotFoundException;
import com.booking.model.Route;
import com.booking.service.RouteService;

 

@RunWith(MockitoJUnitRunner.Silent.class)
public class RouterControllerTest {

 

    @InjectMocks
    RouteController routeController;

 

    @Mock
    RouteService routeService;

 

    @Test
    public void TestRouteForPositive() {

 

        ResponseDto response = new ResponseDto();
        response.setMessage("success");

 

        RouteDto route = new RouteDto();
        route.setDestination("chennai");
        route.setSource("bangalore");

 

        Mockito.when(routeService.saveRoute((Mockito.any()), null).thenReturn(Mockito.any()));

 

        ResponseEntity<ResponseDto> result = routeController.route(route, null);
        assertNotNull(result);
    }
    
    @Test
    public void TestGetAllForPositive() throws AdminNotFoundException {

 

        List<Route> route = new ArrayList<>();
        Route rout = new Route();
        rout.setDestination("chennai");
        rout.setRouteId(1);
        rout.setSource("bangalore");

 

        route.add(rout);

 

        Mockito.when(routeService.getAllRoutes()).thenReturn(route);
        List<Route> result = routeService.getAllRoutes();
        assertNotNull(result);

 

    }

 

    @Test
    public void TestUpdateRouteForPositive() throws AdminNotFoundException {

 

        ResponseDto response = new ResponseDto();
        response.setMessage("success");

 

        RouteDto route = new RouteDto();
        route.setDestination("chennai");
        route.setSource("bangalore");

 

        Mockito.when(routeService.updateRoute(Mockito.any(), null)).thenReturn(Mockito.any());

 

        ResponseEntity<ResponseDto> result = routeController.updateRoute(route, null);
        assertNotNull(result);



    }


}
