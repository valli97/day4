package com.booking.controllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.booking.controller.UserRegistrationController;
import com.booking.dto.ResponseDto;
import com.booking.dto.UserRegistrationDto;
import com.booking.model.User;
import com.booking.service.UserRegistrationService;

@RunWith(MockitoJUnitRunner.class)
public class UserRegistrationControllerTest {
	@InjectMocks
	UserRegistrationController userRegistrationController;

	@Mock
	UserRegistrationService userRegistrationService;

	@Test
	public void TestUserRegistrationForPositive() {

		UserRegistrationDto regdto = new UserRegistrationDto();
		regdto.setUserId(1);
		regdto.setUserName("kumar");
		regdto.setPassword("kuara980");
		regdto.setEmailId("guru@gmail.com");
		regdto.setMobileNumber("8870448827");

		ResponseDto resdto = new ResponseDto();
		resdto.setMessage("success");

		Mockito.when(userRegistrationService.saveUserRegistration(Mockito.any())).thenReturn(Mockito.any());

		ResponseEntity<ResponseDto> reg = userRegistrationController.userRegistration(regdto);
		Assert.assertNotNull(reg);
		Assert.assertEquals(HttpStatus.CREATED, reg.getStatusCode());

	}
	@Test
    public void TestUserDetailsForPositive() {
        
        User user1=new User();
        user1.setEmailId("dhaya@gmail.com");
        user1.setMobileNumber("7893458398");
        user1.setPassword("dhanraj79");
        user1.setUserId(2);
        user1.setUserName("dhanaraj");
        
        Mockito.when(userRegistrationService.passengerDetails((int) Mockito.anyLong())).thenReturn(user1);
        
        ResponseEntity<User> result=userRegistrationController.userDetails((Mockito.anyInt()));
        assertNotNull(result);
        assertEquals( HttpStatus.OK, result.getStatusCode());
    }
}
  

