package com.booking.controllerTest;

 

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

 

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

 

import com.booking.controller.TicketBookingController;
import com.booking.dto.TicketBookingDto;
import com.booking.dto.TicketDto;
import com.booking.service.TicketBookingService;

 

@RunWith(MockitoJUnitRunner.Silent.class)
public class TicketBookingControllerTest {

 

    @InjectMocks
    TicketBookingController ticketBookingController;

 

    @Mock
    TicketBookingService bookingService;
    
    @Test
    public void TestBookingTheTicketForPositive() {
        
        TicketBookingDto booking3 = new TicketBookingDto();
        booking3.setNumberofSeat(45);
        booking3.setReservationDate("2020-12-03");
        booking3.setTicketId(1);
        booking3.setTicketNumber(12);
        
        TicketDto ticket=new TicketDto();
        ticket.setBusId(1);
        ticket.setNumberofSeat(3);
        ticket.setReservationDate("2020-12-03");
        ticket.setTicketId(1);
        ticket.setTicketNumber(12);
        ticket.setUserId(1);
        
        Mockito.when(bookingService.bookTheSeat(Mockito.any())).thenReturn(booking3);
        
        ResponseEntity<TicketBookingDto> result=ticketBookingController.bookingtheTicket(ticket);
        assertEquals(HttpStatus.CREATED,result.getStatusCode()); 
    }

 

}
